DownloadAndExpandFile(){

    wget $1
    echo "Descompactando arquivos"
    unzip $2 -d "$3"
}

CSV_DIR="./csv"
SHP_DIR="./shp"

rm -rf $CSV_DIR
rm -rf $SHP_DIR
mkdir -p $CSV_DIR
mkdir -p $SHP_DIR

APOLICES_URL="https://gitlab.com/jralmeida/shared_data/-/raw/main/apolices.zip"
APOLIICES_ZIP="./apolices.zip"
APOLICES_TABLE_UN="apolices_un"
DownloadAndExpandFile $APOLICES_URL $APOLIICES_ZIP $CSV_DIR



TI_URL="https://gitlab.com/jralmeida/shared_data/-/raw/main/ti_sirgas.zip"
TI_ZIP="./ti_sirgas.zip"
DownloadAndExpandFile $TI_URL $TI_ZIP $SHP_DIR


TRABALHO_ESCRAVO_URL="https://gitlab.com/jralmeida/shared_data/-/raw/main/trabalho_escravo.zip"
TRABALHO_ESCRAVO_ZIP="./trabalho_escravo.zip"
DownloadAndExpandFile $TRABALHO_ESCRAVO_URL $TRABALHO_ESCRAVO_ZIP $CSV_DIR


echo "#### Solos IBGE #################################################"
SOLOS_IBGE_URL="https://gitlab.com/jralmeida/shared_data/-/raw/main/solos_ibge.zip"
SOLOS_IBGE_ZIP="./solos_ibge.zip"
DownloadAndExpandFile $SOLOS_IBGE_URL $SOLOS_IBGE_ZIP $SHP_DIR




rm -rf *.zip
rm -rf *.zip.*