#!/bin/bash
docker rm -f $(docker ps -aq)
docker image rm $(docker image ls -q)
docker build -t banco-mapa .
docker run --name="banco-mapa" -d -p 5432:5432 banco-mapa 
sleep 10
docker exec banco-mapa /banco/run.sh
git clone https://github.com/ufsj-mapa/mapa-script