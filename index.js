var exec = require("child_process").execSync
var script = "bash"
if(process.platform === "win32"){
    script = "powershell.exe"
}
console.log("Buildando imagem")
exec("docker build -t banco-mapa .", {shell:script,stdio:"inherit"})
console.log("Imagem buildada")
console.log("Iniciando container")
exec('docker run --name="banco-mapa" -d -p 5432:5432 banco-mapa',{shell:script,stdio:"inherit"})
console.log("Container iniciado")
console.log("Esperando conexão com postgres")
exec('sleep 10',{shell:script,stdio:"inherit"})

exec('docker exec banco-mapa /banco/run.sh',{shell:script,stdio:"inherit"})
console.log("Banco de dados iniciado")
console.warn("\n Banco criado com sucesso com as seguintes configurações: \nporta: 5432\nhost: localhost\nusuario: postgres\nsenha: localdb\n. O banco esta rodando no container banco-mapa com porta mapeada para 5432");
console.log("Clonando repositório com aplicacoes");
console.log("Caso falhe clone o repositorio na url: https://github.com/ufsj-mapa/mapa-script")
exec('git clone https://github.com/ufsj-mapa/mapa-script',{shell:script,stdio:"inherit"})


        
