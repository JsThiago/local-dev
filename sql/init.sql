CREATE EXTENSION postgis;
CREATE EXTENSION postgis_raster;
CREATE EXTENSION postgis_topology;
CREATE EXTENSION postgis_sfcgal;
CREATE EXTENSION fuzzystrmatch;
CREATE EXTENSION address_standardizer;
CREATE EXTENSION address_standardizer_data_us;
CREATE EXTENSION postgis_tiger_geocoder;

CREATE TABLE apolices_un
(
    nm_razao_social           varchar(255),
    cd_processo_susep         bigint,
    nr_proposta               varchar(255),
    id_proposta               bigint,
    dt_proposta               varchar(255),
    dt_inicio_vigencia        varchar(255),
    dt_fim_vigencia           varchar(255),
    nm_segurado               varchar(255),
    nr_documento_segurado     varchar(255),
    nm_municipio_propriedade  varchar(255),
    sg_uf_propriedade         varchar(255),
    latitude                  varchar(255),
    nr_grau_lat               integer,
    nr_min_lat                integer,
    nr_seg_lat                integer,
    longitude                 varchar(255),
    nr_grau_long              integer,
    nr_min_long               integer,
    nr_seg_long               integer,
    nm_classif_produto        varchar(255),
    nm_cultura_global         varchar(255),
    nr_area_total             double precision,
    nr_animal                 double precision,
    nr_produtividade_estimada double precision,
    nr_produtividade_segurada double precision,
    nivel_cobertura           double precision,
    vl_limite_garantia        double precision,
    vl_premio_liquido         double precision,
    pe_taxa                   double precision,
    vl_subvencao_federal      double precision,
    nr_apolice                varchar(255),
    dt_apolice                varchar(255),
    ano_apolice               bigint,
    cd_geocmu                 bigint,
    valor_indenizacao         double precision,
    evento_preponderante      varchar(255)
);

CREATE TABLE trabalho_escravo_un
(
    id                     smallint PRIMARY KEY,
    ano_acao_fiscal        smallint     NOT NULL,
    uf                     varchar(2)   NOT NULL,
    empregador             varchar(255) NOT NULL,
    cnpj_cpf               varchar(25)  NOT NULL,
    estabelecimento        varchar(255) NOT NULL,
    trabalhadores          smallint     NOT NULL,
    cnae                   varchar(25)  NOT NULL,
    decisao_administrativa varchar(255) NOT NULL,
    inclusao               varchar(255) NOT NULL
);
