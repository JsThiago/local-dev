CREATE TABLE apolices
(
    id_proposta               bigint PRIMARY KEY,
    nm_razao_social           varchar(50),
    cd_processo_susep         bigint,
    nr_proposta               varchar(25),
    dt_proposta               date,
    dt_inicio_vigencia        date,
    dt_fim_vigencia           date,
    nm_segurado               varchar(100),
    nr_documento_segurado     varchar(20),
    nm_municipio_propriedade  varchar(50),
    sg_uf_propriedade         char(2),
    latitude                  char(1),
    nr_grau_lat               integer,
    nr_min_lat                integer,
    nr_seg_lat                integer,
    longitude                 char(1),
    nr_grau_long              integer,
    nr_min_long               integer,
    nr_seg_long               integer,
    nm_classif_produto        varchar(15),
    nm_cultura_global         varchar(15),
    nr_area_total             double precision,
    nr_animal                 double precision,
    nr_produtividade_estimada double precision,
    nr_produtividade_segurada double precision,
    nivel_cobertura           double precision,
    vl_limite_garantia        double precision,
    vl_premio_liquido         double precision,
    pe_taxa                   double precision,
    vl_subvencao_federal      double precision,
    nr_apolice                varchar(25),
    dt_apolice                date,
    ano_apolice               smallint,
    cd_geocmu                 bigint,
    valor_indenizacao         double precision,
    evento_preponderante      varchar(35)
);

INSERT INTO apolices(id_proposta,
                     nm_razao_social,
                     cd_processo_susep,
                     nr_proposta,
                     dt_proposta,
                     dt_inicio_vigencia,
                     dt_fim_vigencia,
                     nm_segurado,
                     nr_documento_segurado,
                     nm_municipio_propriedade,
                     sg_uf_propriedade,
                     latitude,
                     nr_grau_lat,
                     nr_min_lat,
                     nr_seg_lat,
                     longitude,
                     nr_grau_long,
                     nr_min_long,
                     nr_seg_long,
                     nm_classif_produto,
                     nm_cultura_global,
                     nr_area_total,
                     nr_animal,
                     nr_produtividade_estimada,
                     nr_produtividade_segurada,
                     nivel_cobertura,
                     vl_limite_garantia,
                     vl_premio_liquido,
                     pe_taxa,
                     vl_subvencao_federal,
                     nr_apolice,
                     dt_apolice,
                     ano_apolice,
                     cd_geocmu,
                     valor_indenizacao,
                     evento_preponderante)
SELECT id_proposta,
       nm_razao_social,
       cd_processo_susep,
       nr_proposta,
       to_date(dt_proposta, 'DD/MM/YYYY'),
       to_date(dt_inicio_vigencia, 'DD/MM/YYYY'),
       to_date(dt_fim_vigencia, 'DD/MM/YYYY'),
       nm_segurado,
       nr_documento_segurado,
       nm_municipio_propriedade,
       sg_uf_propriedade,
       latitude,
       nr_grau_lat,
       nr_min_lat,
       nr_seg_lat,
       longitude,
       nr_grau_long,
       nr_min_long,
       nr_seg_long,
       nm_classif_produto,
       nm_cultura_global,
       nr_area_total,
       nr_animal,
       nr_produtividade_estimada,
       nr_produtividade_segurada,
       nivel_cobertura,
       vl_limite_garantia,
       vl_premio_liquido,
       pe_taxa,
       vl_subvencao_federal,
       nr_apolice,
       to_date(dt_apolice, 'DD/MM/YYYY'),
       ano_apolice,
       cd_geocmu,
       valor_indenizacao,
       evento_preponderante
FROM apolices_un;

DROP TABLE apolices_un;

ALTER TABLE apolices
    ADD COLUMN log_gd double precision,
    ADD COLUMN lat_gd double precision;

CREATE FUNCTION double_coordinate(dir char(1), deg integer, min integer, sec integer)
    RETURNS numeric
AS
$$
SELECT CASE
           WHEN dir = 'N' THEN
               (deg + min / 60.0 + sec / 3600.0)
           WHEN dir = 'S' THEN
               (deg + min / 60.0 + sec / 3600.0) * - 1
           WHEN dir = 'E' THEN
               (deg + min / 60.0 + sec / 3600.0)
           WHEN dir = 'W' THEN
               (deg + min / 60.0 + sec / 3600.0) * - 1
           ELSE
               NULL
           END
$$
    LANGUAGE sql;

UPDATE apolices
SET log_gd = double_coordinate(longitude, nr_grau_long, nr_min_long, nr_seg_long),
    lat_gd = double_coordinate(latitude, nr_grau_lat, nr_min_lat, nr_seg_lat);

ALTER TABLE apolices
    DROP COLUMN latitude,
    DROP COLUMN longitude,
    DROP COLUMN nr_grau_lat,
    DROP COLUMN nr_min_lat,
    DROP COLUMN nr_seg_lat,
    DROP COLUMN nr_grau_long,
    DROP COLUMN nr_min_long,
    DROP COLUMN nr_seg_long;

ALTER TABLE apolices
    RENAME COLUMN log_gd TO longitude;
ALTER TABLE apolices
    RENAME COLUMN lat_gd TO latitude;

CREATE TABLE apolices_pontos
(
    id_proposta bigint PRIMARY KEY,
    geom        geometry(Point, 4674),
    CONSTRAINT fk_apolices_pontos_apolices
        FOREIGN KEY (id_proposta)
            REFERENCES apolices (id_proposta)
);

CREATE INDEX apolices_pontos_gix
    ON apolices_pontos
        USING GIST (geom);

INSERT INTO apolices_pontos (id_proposta, geom)
SELECT id_proposta,
       ST_SetSRID(ST_MakePoint(longitude, latitude), 4674)
FROM apolices;


CREATE TABLE apolices_buffers
(
    id_proposta bigint PRIMARY KEY,
    raio        double precision,
    geom        geometry(Polygon, 4674),
    CONSTRAINT fk_apolices_buffers_apolices
        FOREIGN KEY (id_proposta)
            REFERENCES apolices (id_proposta)
);


CREATE INDEX apolices_buffers_gix
    ON apolices_buffers
        USING GIST (geom);

INSERT INTO apolices_buffers (id_proposta, raio, geom)
SELECT ap.id_proposta,
       SQRT(nr_area_total * 10000 / PI()) AS raio,
       ST_GeomFromText(
               ST_AsText(
                       ST_Buffer(
                               ap_pontos.geom::geography,
                               SQRT(nr_area_total * 10000 / PI())
                           )
                   ),
               4674
           )
FROM apolices_pontos as ap_pontos,
     apolices as ap
WHERE ap.id_proposta = ap_pontos.id_proposta;

CREATE TABLE trabalho_escravo
(
    id                     smallint PRIMARY KEY,
    ano_acao_fiscal        smallint     NOT NULL,
    uf                     varchar(2)   NOT NULL,
    empregador             varchar(255) NOT NULL,
    cnpj_cpf               varchar(14)  NOT NULL,
    estabelecimento        varchar(255) NOT NULL,
    trabalhadores          smallint     NOT NULL,
    cnae                   varchar(7)   NOT NULL,
    decisao_administrativa date         NOT NULL,
    inclusao               date         NOT NULL
);

INSERT INTO trabalho_escravo(id,
                             ano_acao_fiscal,
                             uf,
                             empregador,
                             cnpj_cpf,
                             estabelecimento,
                             trabalhadores,
                             cnae,
                             decisao_administrativa,
                             inclusao)
SELECT id,
       ano_acao_fiscal,
       uf,
       empregador,
       replace(replace(replace(cnpj_cpf, '.', ''), '/', ''), '-', '')    AS cnpj_cpf,
       estabelecimento,
       trabalhadores,
       replace(replace(cnae, '/', ''), '-', '')                          AS cnae,
       to_date(split_part(decisao_administrativa, ' ', 1), 'DD/MM/YYYY') AS decisao_administrativa,
       to_date(split_part(inclusao, ' ', 1), 'DD/MM/YYYY')               AS inclusao
FROM trabalho_escravo_un;

DROP TABLE trabalho_escravo_un;

CREATE TABLE socioambientais
(
    id     SERIAL PRIMARY KEY,
    gid    INTEGER,
    tipo   VARCHAR(100),
    nome   VARCHAR(255),
    codigo integer,
    geom   geometry(MultiPolygon, 4674)
);

CREATE INDEX socioambientais_gix
    ON socioambientais
        USING GIST (geom);

CREATE VIEW inter_soc_amb_view AS
SELECT soc_amb.tipo                                   AS soc_amb,
       soc_amb.nome                                   AS soc_amb_nome,
       soc_amb.codigo                                 AS soc_amb_cod,
       ap.id_proposta                                 AS id_proposta,
       ap.nm_segurado                                 AS nm_segurado,
       ap.nm_razao_social                             AS nm_razao_social,
       ap.nm_cultura_global                           AS nm_cultura_global,
       ap.nr_area_total                               AS nr_area_total,
       ap.nm_municipio_propriedade                    AS nm_municipio_propriedade,
       ap.sg_uf_propriedade                           AS sg_uf_propriedade,
       ap_buffers.geom                                AS geom_ap,
       soc_amb.geom                                   AS geom_soc_amb,
       st_intersection(soc_amb.geom, ap_buffers.geom) AS geom_inter
FROM apolices_buffers AS ap_buffers,
     socioambientais AS soc_amb,
     apolices AS ap
WHERE st_intersects(soc_amb.geom, ap_buffers.geom)
  AND ap_buffers.id_proposta = ap.id_proposta
  AND ap.nr_area_total > 0;

DELETE
FROM terras_indigenas
WHERE dominio_un != 't';

INSERT INTO socioambientais (gid, tipo, nome, codigo, geom)
SELECT gid,
       'Terra indígena' AS tipo,
       terrai_nom       AS nome,
       terrai_cod       AS codigo,
       geom
FROM terras_indigenas;

-- DELETE
-- FROM embargos_ibama
-- WHERE geom IS NULL;

-- INSERT INTO socioambientais (gid, tipo, nome, codigo, geom)
-- SELECT gid,
--        'Embargo IBAMA'   AS tipo,
--        nom_pessoa        AS nome,
--        objectid::integer AS codigo,
--        geom
-- FROM embargos_ibama;

-- INSERT INTO socioambientais (gid, tipo, nome, codigo, geom)
-- SELECT gid,
--        'Embargo ICMBio' AS tipo,
--        autuado          AS nome,
--        vw_pk::integer   AS codigo,
--        geom
-- FROM embargos_icmbio;

-- DELETE
-- FROM unidades_conservacao
-- WHERE categori3 NOT IN
--       ('Estação Ecológica', 'Reserva Biológica', 'Parque', 'Monumento Natural', 'Refúgio de Vida Silvestre');

-- INSERT INTO socioambientais (gid, tipo, nome, codigo, geom)
-- SELECT gid,
--        'Unidade de Conservação' AS tipo,
--        nome_uc1                 AS nome,
--        id_uc0::integer          As codigo,
--        geom
-- FROM unidades_conservacao;

-- INSERT INTO socioambientais (gid, tipo, nome, codigo, geom)
-- SELECT gid,
--        'Áreas quilombolas'                   AS tipo,
--        coalesce(nm_comunid, 'Não informado') AS nome,
--        coalesce(cd_quilomb::integer, 0)      AS codigo,
--        geom                                  AS geom
-- FROM areas_quilombolas;

-- SELECT count(*) AS total
-- FROM socioambientais;

ALTER TABLE solos_ibge
    ADD COLUMN textura_sisdagro VARCHAR(10);

UPDATE solos_ibge
SET textura_sisdagro =
        CASE
            WHEN textura LIKE '%arenosa%' THEN 'Arenoso'
            WHEN textura LIKE '%média%' OR textura LIKE '%siltosa%' THEN 'Médio'
            WHEN textura LIKE '%argilosa%' OR textura LIKE '%indiscriminada%' THEN 'Argiloso'
            ELSE 'Arenoso'
            END;

ALTER TABLE solos_ibge
    ADD COLUMN textura_zarc VARCHAR(10);

UPDATE solos_ibge
SET textura_zarc =
        CASE
            WHEN textura LIKE '%argilosa%' THEN 'Argiloso'
            WHEN textura LIKE '%média%' OR textura LIKE '%siltosa%' THEN 'Médio'
            WHEN textura LIKE '%arenosa%' THEN 'Arenoso'
            ELSE 'Argiloso'
            END;
