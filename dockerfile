FROM postgis/postgis
ENV POSTGRES_PASSWORD=localdb
ENV POSTGRES_USER=postgres
ENV POSTGRES_DB=local_mapapg
RUN apt-get -y update
RUN apt-get -y install unzip
RUN apt-get -y install wget
RUN apt-get -y install postgis
COPY ./sql /banco/sql
COPY ./run.sh /banco
COPY ./Download.sh /banco
