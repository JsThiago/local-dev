#!/bin/bash
cd /banco
set -e
SQL_DIR="sql"
CSV_DIR="csv"
SHP_DIR="shp"
DB_HOST="localhost"
DB_PORT=5432
DB_USER="postgres"
DB_NAME="local_mapapg"
DB_PASS="localdb"
INIT_SQL="$SQL_DIR/init.sql"
/banco/Download.sh


#Parametros:$TABLE_UN,$CSV
Import-Csvfile(){
   echo "Importando arquivo csv"
   if psql -U $DB_USER -d $DB_NAME -h $DB_HOST -p $DB_PORT -c "\copy $1 from $2 with (format csv,header true, delimiter ';', encoding utf8);"; then
       echo "csv importado"
   else
       echo "Erro ao importar csv" >&2
       exit
   fi
   }


#Parametros:$SHP,$TABLE,$ENCODING,$SRID
Import-Shapefile(){
    echo "Importando shapefile"
    if [! -z $3 ]; then
       echo "Skipped"
    else
       shp2pgsql -W "LATIN1" -I -s "4674" $1 $2 | psql -d $DB_NAME -h $DB_HOST -p $DB_PORT -U $DB_USER 
    fi
    
    if [ "$?" -ne 0 ]; then
        echo "Erro ao importar Shapefile"
        exit
    fi
}


echo "Habilitando PostGIS e iniciando o banco de dados"
psql -U $DB_USER -p $DB_PORT -h $DB_HOST -d $DB_NAME -f $INIT_SQL



echo "#### Apolices ###################################################"
APOLICES_CSV="./$CSV_DIR/apolices.csv"
APOLICES_TABLE_UN="apolices_un"
Import-Csvfile $APOLICES_TABLE_UN $APOLICES_CSV


echo "#### Trabalho escravo ###########################################"
TRABALHO_ESCRAVO_CSV="./$CSV_DIR/trabalho_escravo.csv"
TRABALHO_ESCRAVO_TABLE_UN="trabalho_escravo_un"
Import-Csvfile $TRABALHO_ESCRAVO_TABLE_UN $TRABALHO_ESCRAVO_CSV


echo "#### Terras indigenas ###########################################"
TI_SHP="./$SHP_DIR/ti_sirgas.shp"
TI_TABLE="terras_indigenas"
Import-Shapefile $TI_SHP $TI_TABLE


echo "#### Solos IBGE #################################################"
SOLOS_IBGE_SHP="./$SHP_DIR/pedo_area.shp"
SOLOS_IBGE_TABLE="solos_ibge"
Import-Shapefile $SOLOS_IBGE_SHP $SOLOS_IBGE_TABLE


PREPARE_SQL="./$SQL_DIR/prepare.sql"
psql -d $DB_NAME -h $DB_HOST -p $DB_PORT -U $DB_USER -f $PREPARE_SQL

echo $'\n'
echo '#### Processo concluido #########################################'
echo $'\n'
