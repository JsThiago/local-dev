# LOCAL-DEV

## Executando no docker

**Para rodar o codigo sao necessarias as seguintes ferramentas:**

- ***docker***
- ***git***
- ***nodejs***

Para executar bastar executar no powershell ou terminal ***node ./index.js***

**O index.js somente builda a imagem, cria o container e roda o script run.sh dentro do container.**
**Caso o index.js de problema voce pode seguir esses passos. O script index.js possui todos os comandos que devem ser rodados em ordem. Esses comandos sao os primeiros parametros das funcoes exec.**



## Executando fora do docker

### Linux

**No linux voce deve instalar as seguintes ferramentas antes de compilar:**

- ***Postgres (cliente e servidor)***
- ***Postgis***
- ***Ferramentas unix como wget e unzip***

**Para alguma informacao acesse o arquivo dockerfile para ter uma ideia de quais ferramentas instalar. Lembrando que o postgres ja vem instalando na imagem. Portanto o postgres nao e instalado no arquivo dockerfile.**

Voce pode compilar fora do docker executando o script run.sh no linux (terminal).

### Windows

***O script para windows ja instala as ferramentas necessarias.***

Voce pode compilar fora do docker executando o script run.ps1 no windows (powershell).







