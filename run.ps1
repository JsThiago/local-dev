function New-Line () {
    Write-Output "`n"
}

Write-Output "Ferramenta responsavel por popular a base de dados local"
Write-Output "Algumas confirmacoes poderao ser necessarias durante a execucao"
New-Line

$dbhost = "localhost"
$dbport = "5432"
$dbname = "local_mapapg"
$dbuser = "postgres"
$password = Read-Host -Prompt "Digite a senha do postgres"
New-Line

$tools_dir = "tools"
Write-Output "Criando diretorio de binarios: *$tools_dir"
if (Test-Path -d $tools_dir) {
    Write-Output "Diretorio ja existe, pulando etapa"
}
else {
    New-Item -ItemType Directory -Force -Path $tools_dir

    $url = "https://get.enterprisedb.com/postgresql/postgresql-14.2-1-windows-x64-binaries.zip"
    $filepath = "$tools_dir/postgresql-14.2-1-windows-x64-binaries.zip"
    Write-Output "Baixando binarios do PostgreSQL: *$url"
    Write-Output "Salvando em: *$filepath"
    Invoke-WebRequest -Uri $url -OutFile $filepath
    Write-Output "Descompactando binarios em *$tools_dir/pgsql"
    Expand-Archive -Path $filepath -DestinationPath $tools_dir

    $url = "http://download.osgeo.org/postgis/windows/pg14/postgis-bundle-pg14-3.2.0x64.zip"
    $filepath = "$tools_dir/postgis-bundle-pg14-3.2.0x64.zip"
    Write-Output "Baixando binarios do PostGIS: *$url"
    Write-Output "Salvando em: *$filepath"
    Invoke-WebRequest -Uri $url -OutFile $filepath
    Write-Output "Descompactando binarios em *$tools_dir/postgis-bundle-pg14-3.2.0x64"
    Expand-Archive -Path $filepath -DestinationPath $tools_dir
}
New-Line

$sql_dir = "sql"
$csv_dir = "csv"
$shp_dir = "shp"
$dirs = @($csv_dir, $shp_dir)
foreach ($dir in $dirs) {
    Write-Output "Criando diretorio: *$dir"
    if (Test-Path -d $dir) {
        Write-Output "Diretorio ja existe e sera recriado"
        Remove-Item -Recurse -Force $dir
    }
    New-Item -ItemType Directory -Force -Path $dir
}
New-Line

$psql_bin = ".\$tools_dir\pgsql\bin\psql.exe"
$dropdb_bin = ".\$tools_dir\pgsql\bin\dropdb.exe"
$createdb_bin = ".\$tools_dir\pgsql\bin\createdb.exe"
$shp2pgsql_bin = ".\$tools_dir\postgis-bundle-pg14-3.2.0x64\bin\shp2pgsql.exe"

Set-Item -Path Env:PGPASSWORD -Value $password

Write-Output "Criando um novo banco de dados vazio: *$dbname"
& $dropdb_bin --if-exists -h $dbhost -p $dbport -U $dbuser $dbname
& $createdb_bin -h $dbhost -p $dbport -U $dbuser -E UTF8 $dbname
if ($LastExitCode -ne 0) {
    Write-Output "Erro ao criar o banco de dados"
    Write-Output "- garanta que nao ha uma conexao aberta com o banco de dados de nome local_mapapg"
    Write-Output "- ou tente deletar o banco de dados manualmente"
    exit 1
}

$init_sql = ".\$sql_dir\init.sql"
Write-Output "Habilitando PostGIS e iniciando o banco de dados"
& $psql_bin -d $dbname -h $dbhost -p $dbport -U $dbuser -f $init_sql

function DownloadAndExpandFile ($url, $zip, $dir) {
    Invoke-WebRequest -Uri $url -OutFile $zip
    Write-Information "Descompactando arquivo"
    Expand-Archive -Path $zip -DestinationPath $dir -Force
}

function Import-Csvfile {
    param (
        [string] $url,
        [string] $zip,
        [string] $table_un,
        [string] $csv
    )
    Write-Information "Efetuando download do arquivo csv"
    DownloadAndExpandFile -url $url -zip $zip -dir $csv_dir
    Write-Information "Importando arquivo csv"
    & $psql_bin -d $dbname -h $dbhost -p $dbport -U $dbuser -c "\copy $table_un from '$csv' with (format csv,header true, delimiter ';', encoding utf8);"
    if ($LastExitCode -ne 0) {
        Write-Output "!!! Processo encerrado devido a um erro durante a importacao do arquivo csv"
        exit 1
    }
}

function Import-Shapefile {
    param (
        [string] $url,
        [string] $zip,
        [string] $shp,
        [string] $table,
        [string] $encoding = "LATIN1",
        [string] $srid = "4674"
    )
    Write-Information "Efetuando download do shapefile"
    DownloadAndExpandFile -url $url -zip $zip -dir $shp_dir
    Write-Information "Importando shapefile"
    if ($encoding -eq "*") {
        # & $shp2pgsql_bin -I -s $srid $shp $table | & $psql_bin -d $dbname -h $dbhost -p $dbport -U $dbuser
        Write-Output "Skipped"
    }
    else {
        & $shp2pgsql_bin -W $encoding -I -s $srid $shp $table | & $psql_bin -d $dbname -h $dbhost -p $dbport -U $dbuser
    }
    if ($LastExitCode -ne 0) {
        Write-Output "!!! Processo encerrado devido a um erro durante a importacao do shapefile"
        exit 1
    }
}

Write-Output "#### Apolices ###################################################"
$apolices_url = "https://gitlab.com/jralmeida/shared_data/-/raw/main/apolices.zip"
$apolices_zip = ".\$csv_dir\apolices.zip"
$apolices_csv = ".\$csv_dir\apolices.csv"
$apolices_table_un = "apolices_un"
Import-Csvfile -url $apolices_url -zip $apolices_zip -table_un $apolices_table_un -csv $apolices_csv

Write-Output "#### Trabalho escravo ###########################################"
$trabalho_escravo_url = "https://gitlab.com/jralmeida/shared_data/-/raw/main/trabalho_escravo.zip"
$trabalho_escravo_zip = ".\$csv_dir\trabalho_escravo.zip"
$trabalho_escravo_csv = ".\$csv_dir\trabalho_escravo.csv"
$trabalho_escravo_table_un = "trabalho_escravo_un"
Import-Csvfile -url $trabalho_escravo_url -zip $trabalho_escravo_zip -table_un $trabalho_escravo_table_un -csv $trabalho_escravo_csv

Write-Output "#### Terras indigenas ###########################################"
$ti_url = "https://gitlab.com/jralmeida/shared_data/-/raw/main/ti_sirgas.zip"
$ti_zip = ".\$shp_dir\ti_sirgas.zip"
$ti_shp = ".\$shp_dir\ti_sirgas.shp"
$ti_table = "terras_indigenas"
Import-Shapefile -url $ti_url -zip $ti_zip -shp $ti_shp -table $ti_table

# Write-Output "#### Embargos IBAMA #############################################"
# $embargos_ibama_url = "https://gitlab.com/jralmeida/shared_data/-/raw/main/embargos_ibama.zip"
# $embargos_ibama_zip = ".\$shp_dir\embargos_ibama.zip"
# $embargos_ibama_shp = ".\$shp_dir\embargos_ibama.shp"
# $embargos_ibama_table = "embargos_ibama"
# Import-Shapefile -url $embargos_ibama_url -zip $embargos_ibama_zip -shp $embargos_ibama_shp -table $embargos_ibama_table

# Write-Output "#### Embargos ICMBio ############################################"
# $embargos_icmbio_url = "https://gitlab.com/jralmeida/shared_data/-/raw/main/embargos_icmbio.zip"
# $embargos_icmbio_zip = ".\$shp_dir\embargos_icmbio.zip"
# $embargos_icmbio_shp = ".\$shp_dir\embargos_icmbio.shp"
# $embargos_icmbio_table = "embargos_icmbio"
# Import-Shapefile -url $embargos_icmbio_url -zip $embargos_icmbio_zip -shp $embargos_icmbio_shp -table $embargos_icmbio_table

# Write-Output "#### Unidades de Conservacao ####################################"
# $unidades_conservacao_url = "https://gitlab.com/jralmeida/shared_data/-/raw/main/unidades_conservacao.zip"
# $unidades_conservacao_zip = ".\$shp_dir\unidades_conservacao.zip"
# $unidades_conservacao_shp = ".\$shp_dir\unidades_conservacao.shp"
# $unidades_conservacao_table = "unidades_conservacao"
# Import-Shapefile -url $unidades_conservacao_url -zip $unidades_conservacao_zip -shp $unidades_conservacao_shp -table $unidades_conservacao_table

# Write-Output "#### Sitios arqueologicos #######################################"
# $sitios_arqueologicos_url = "https://gitlab.com/jralmeida/shared_data/-/raw/main/sitios.zip"
# $sitios_arqueologicos_zip = ".\$shp_dir\sitios.zip"
# $sitios_arqueologicos_shp = ".\$shp_dir\sitios.shp"
# $sitios_arqueologicos_table = "sitios_arqueologicos"
# Import-Shapefile -url $sitios_arqueologicos_url -zip $sitios_arqueologicos_zip -shp $sitios_arqueologicos_shp -table $sitios_arqueologicos_table

# Write-Output "#### Areas quilombolas ##########################################"
# $areas_quilombolas_url = "https://gitlab.com/jralmeida/shared_data/-/raw/main/areas_quilombolas.zip"
# $areas_quilombolas_zip = ".\$shp_dir\areas_quilombolas.zip"
# $areas_quilombolas_shp = ".\$shp_dir\areas_quilombolas.shp"
# $areas_quilombolas_table = "areas_quilombolas"
# Import-Shapefile -url $areas_quilombolas_url -zip $areas_quilombolas_zip -shp $areas_quilombolas_shp -table $areas_quilombolas_table

Write-Output "#### Solos IBGE #################################################"
$solos_ibge_url = "https://gitlab.com/jralmeida/shared_data/-/raw/main/solos_ibge.zip"
$solos_ibge_zip = ".\$shp_dir\solos_ibge.zip"
$solos_ibge_shp = ".\$shp_dir\pedo_area.shp"
$solos_ibge_table = "solos_ibge"
Import-Shapefile -url $solos_ibge_url -zip $solos_ibge_zip -shp $solos_ibge_shp -table $solos_ibge_table -encoding "*"
.\tools\postgis-bundle-pg14-3.2.0x64\bin\shp2pgsql.exe -I -s 4674 $solos_ibge_shp $solos_ibge_table >> .\shp\solos_ibge.sql
.\tools\pgsql\bin\psql.exe -d $dbname -h $dbhost -p $dbport -U $dbuser  -f .\shp\solos_ibge.sql

Write-Output "#### Preparando banco de dados para uso #########################"
$prepare_sql = ".\$sql_dir\prepare.sql"
& $psql_bin -d $dbname -h $dbhost -p $dbport -U $dbuser -f $prepare_sql

New-Line
Write-Output "#### Processo concluido #########################################"
New-Line